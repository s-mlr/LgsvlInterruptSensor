# LgsvlInterruptSensor
This repository contains the code and assets for a custom Sensor Plugin for the LGSVL simulator - SimInterruptSensor.

## Description
SimInterruptSensor will listen on bridge input and call a custom callback in the PythonAPI of LGSVL simulator.
This allows to run the simulation from the PythonAPI with a timeout of 0 (indefinitely) and end/interrupt the simulation with a ROS trigger.
The simulation thread currently only evaluates if the simulation has been stopped, when returning from a callback in PythonAPI.

The plugin uses the ```autoware_auto_msgs/msg/SimulatorCommunication.msg```. The sensor plugin supports the following message version:

```python
# SimulatorCommunication.msg
builtin_interfaces/Time stamp
bool interrupt_sim
```

## Build from source
1) Clone repository

2) Copy the SimInterruptSensor folder into Assets/External/Sensors/ inside of your simulator source folder.

3) Follow the instructions to build sensor plugins: https://www.lgsvlsimulator.com/docs/sensor-plugins/

## Install plugin
To use this Sensor Plugin:

1) Copy  
    a) the build result  
    b) the precompiled plugin from this repository  
    into AssetBundles/Sensors inside of your simulator binary folder.

2) On simulation startup, the simulator will load all custom sensor plugin bundles inside AssetBundles/Sensors directory, which will be a valid sensor in a vehicle's configuration JSON

3) Add json configuration (see below) to vehicle of your choosing and launch simulation

# Configuration
No parameters used.

Example sensor config JSON:

```json
{
"type" : "SimInterrupt",
"name" : "SimInterrupt Sensor",    
"params": { "Topic": "/lgsvl_com" }
}
```

## Copyright and License

Copyright (c) 2020 Sebastian Mueller

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
