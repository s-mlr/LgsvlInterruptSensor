/**
* Copyright (c) 2020 Sebastian Mueller
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

using SimpleJSON;
using Simulator.Api;
using Simulator.Bridge;
using Simulator.Sensors.UI;
using Simulator.Utilities;
using System.Collections.Generic;
using UnityEngine;

namespace Simulator.Sensors
{
    [SensorType("SimInterrupt", new System.Type[] { typeof(SimulatorCommunicationData) })]
    public class SimInterruptSensor : SensorBase
    {
        private IBridge Bridge;
        private SimulatorCommunicationData data_ = new SimulatorCommunicationData();

        public override void OnBridgeSetup(IBridge bridge) {
            Bridge = bridge;
            Bridge.AddReader<SimulatorCommunication>(Topic, data => {
				data_.convertFrom(data);
                if (ApiManager.Instance != null) 
				{
                    ApiManager.Instance.AddCustom(transform.parent.gameObject, "SimInterrupt", data_.exportToJSON());
                }
			});
		}

        public override void OnVisualize(Visualizer visualizer)
        {
            Debug.Assert(visualizer != null);
            visualizer.UpdateGraphValues(data_.exportToDict());
        }

        public override void OnVisualizeToggle(bool state)
        {
        }
    }
}
