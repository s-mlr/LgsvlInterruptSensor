/**
* Copyright (c) 2020 Sebastian Mueller
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

using SimpleJSON;
using Simulator.Bridge;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Simulator.Sensors
{
	// Data converter class
    public class SimulatorCommunicationDataConverters : IDataConverter<SimulatorCommunicationData>
    {

        public Func<SimulatorCommunicationData, object> GetConverter(IBridge bridge)
        {
            if (bridge.GetType() == typeof(Bridge.Ros.Bridge))
            {
              return (c) =>
              {
                  return new SimulatorCommunication ()
                  {
                      interrupt_sim = (bool)c.interrupt_sim,
                  };
              };
            }
            throw new System.Exception("SimInterruptSensor not implemented for this bridge type!");
        }

        public Type GetOutputType(IBridge bridge)
        {
            if (bridge.GetType() == typeof(Bridge.Ros.Bridge))
            {
                return typeof(SimulatorCommunication);
            }
            throw new System.Exception("SimInterruptSensor not implemented for this bridge type!");
        }		
    }

	// Internal data container class
	public class SimulatorCommunicationData {
		public long secs;
		public uint nsecs;
		public bool interrupt_sim;

		public SimulatorCommunicationData convertFrom(SimulatorCommunication data) {
			secs = data.stamp.secs;
			nsecs = data.stamp.nsecs;
			interrupt_sim = data.interrupt_sim;
			return this;
		}

		public JSONObject exportToJSON() {
			var jsonData = new JSONObject();
		    jsonData.Add("stamp.secs", this.secs);
	        jsonData.Add("stamp.nsecs", this.nsecs);
	        jsonData.Add("interrupt_sim", this.interrupt_sim);
			return jsonData;	
		}

		public Dictionary<string, object> exportToDict() {
            var graphData = new Dictionary<string, object>() {
                { "time_secs", this.secs },
                { "time_nsecs", this.nsecs },
                { "interrupt_sim", this.interrupt_sim },
            };

			return graphData;	
		}
	}

	// Ros msg container class for data from bridge
    [Bridge.Ros.MessageType("autoware_auto_msgs/SimulatorCommunication")]
    public class SimulatorCommunication
    {
		public Simulator.Bridge.Ros.Time stamp;
        public bool interrupt_sim;
    }

}
